import { Grid, Typography } from '@mui/material'
import { makeStyles } from '@mui/styles'

import { getAllPosts } from '../lib/api'
import { PostType } from '../src/types'

import PostsSection from 'containers/BlogContainers/PostsSection'
import GetInvolvedSection from 'containers/ContributeContainers/GetInvolvedSection'

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: 100
  },
  headingSubTitle: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(6.5)
  },
  contributeBlock: {
    marginTop: theme.spacing(13)
  }
}))

type BlogProps = { allPosts: PostType[]; featuredPosts: PostType[] }

const Blog = ({ allPosts }: BlogProps) => {
  const classes = useStyles()
  return (
    <Grid container className={classes.root} justifyContent="center">
      <Grid
        item
        container
        xs={10}
        justifyContent="center"
        alignItems="center"
        direction="column"
        wrap="nowrap"
      >
        <Typography variant="h1" align="center" paragraph>
          Stealth OS Blog
        </Typography>
        <Typography className={classes.headingSubTitle} variant="subtitle2Semi" align="center">
          Latest Posts
        </Typography>
      </Grid>
      <PostsSection allPosts={allPosts} />
      <GetInvolvedSection />
    </Grid>
  )
}

export default Blog

export const getStaticProps = async () => {
  const allPosts = await getAllPosts([
    'title',
    'date',
    'author',
    'image',
    'description',
    'content',
    'slug'
  ])

  /* const featuredPosts = await Promise.all(
    ['stealth-4.11-release-notes.md', '2020-05-08-stealth-hackthebox.md'].map(slug =>
      getPostBySlug(slug, ['title', 'image', 'date', 'description'], true)
    )
  )*/

  return {
    props: { allPosts }
  }
}
