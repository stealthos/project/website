import { Grid, Typography, Link } from '@mui/material'
import { useTheme } from '@mui/styles'
import SyntaxHighlighter from 'react-syntax-highlighter'
import { a11yLight, a11yDark } from 'react-syntax-highlighter/dist/cjs/styles/hljs'

const Warrant = () => {
  const {
    palette: {
      mode,
      background: { paper }
    }
  } = useTheme()

  const codeStyle = (mode: 'light' | 'dark') =>
    mode === 'light'
      ? { ...a11yLight, ...{ hljs: { ...a11yLight.hljs, backgroundColor: paper } } }
      : { ...a11yDark, ...{ hljs: { ...a11yDark.hljs, backgroundColor: paper } } }

  return (
    <Grid container justifyContent="center">
      <Grid item xs={10} md={8} lg={6}>
        <Typography variant="h1" align="center" paragraph>
          Warrant Canary
        </Typography>
        <Typography variant="h5" paragraph>
          Signed with:
        </Typography>
        <SyntaxHighlighter language="plaintext" style={codeStyle(mode)}>
          {`Lorenzo "Palinuro" Faletra
GPG ID: B350 5059 3C2F 7656 40E6  DDDB 97CA A129 F4C6 B9A4
GPG KEY: https://cdn.palinuro.dev/canary/key-palinuro.gpg
GPG KEY MIRROR: https://deb.stealth.sh/mirrors/stealth/misc/canary/key-palinuro.gpg
GPG KEY MIRROR: https://keybase.io/palinuro/pgp_keys.asc

Stealth Archive Keyring
GPG ID: 813E EFE8 0280 C579 E2A1  F5E6 B56F FA94 6EB1 660A
GPG KEY: https://cdn.palinuro.dev/canary/key-palinuro.gpg
GPG KEY MIRROR: https://deb.stealth.sh/mirrors/stealth/misc/canary/key-palinuro.gpg`}
        </SyntaxHighlighter>
        <Typography variant="h5" paragraph>
          Warrant Canary, July 26 2023
        </Typography>
        <SyntaxHighlighter language="plaintext" style={codeStyle(mode)}>
          {`-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512


Signed Warrant Canary n.5: no incidents or warrants as of July 26 2023


This page is to inform users that Stealth Security has NOT been served with a
secret government subpoena for its servers (Stealth Project, Stealth Security CIC,
Lorenzo Faletra or other directly involved partners), software
(Stealth OS and its official derivatives), or directly owned services
(community portals, git platform, email service, cryptpad service,
cloud platform, CDN nodes, hosted portals etc).

If a new warrant canary has not been updated in the time period specified by
Stealth Security, or if this page disappears, users are to assume that Stealth Project
has indeed been served with a secret subpoena.

The intention is to allow Stealth Security to warn users of the existence of a
subpoena passively, without disclosing to others that the government has sought
or obtained access to information or records under a secret subpoena.

Warrant Canaries have been found to be legal by the United States Justice Department,
so long as they are passive in their notifications.

This message is signed with the GPG keys of the Stealth OS archive keyring and
the Team Leader (and actual legal holder) of Stealth Security.

Every new canary update since July 10 2019 will be digitally signed, and older
versions of the canary will be made available in a public archive.

signed with:

Lorenzo "Palinuro" Faletra
GPG ID: B350 5059 3C2F 7656 40E6  DDDB 97CA A129 F4C6 B9A4
GPG KEY: https://keybase.io/palinuro/pgp_keys.asc
GPG KEY MIRROR: https://deb.stealth.sh/mirrors/stealth/misc/canary/key-palinuro.gpg
GPG KEY MIRROR: https://cdn.palinuro.dev/canary/key-palinuro.gpg

Stealth Archive Keyring
GPG ID: 8B40 60CA 69A9 7356 B2DC  F551 823B F07C EB5C 469B
GPG KEY: https://deb.stealth.sh/mirrors/stealth/misc/canary/key-stealth.gpg
GPG KEY MIRROR: https://cdn.palinuro.dev/canary/key-stealth.gpg


-----BEGIN PGP SIGNATURE-----

iQIzBAEBCgAdFiEEisbhl0C9kVJbQnRMq2tCtrfjQnMFAmTBUYsACgkQq2tCtrfj
QnOy3xAAkpLCfT7HxnXfqIsRAFHn94QAtUYUQjs6Yib8tSm+SLwgYl/0Abf4wnu6
P9LRqn/ZwZq6rkFBOEuY3fNiWzOTkUUF3yOSQQOVznlUTblXi/iW/qhBdI3eGCWO
BEKegBEynYlPgr3cuKLF1Ya9NDaVhR/IMf2+phkTU4a0fgrUwzXILEx0N9zoTeVv
06Ux71FLDeYTim20vquxN26iQ9EfMHKf3GBiDNGvs/4wzWtsySV+rBwT8wjqrSlH
M1zVei3fdoYHr1EmDUiOjGaU+ygWnYV8cnUpPhiETpE5YmcdUmrAEXT/eetov1JC
cWfyMyXIwutK5Wy3A78mVO7fflH+MYmRlkZS29EGq3OptrKIc4Lq311xh1wpHp2g
wQAHzLc706o4+q6uaX8LK+J9mOJs7foXEdI4Inu/0y3L4nl4h32kXdxeoHwZga1x
G+TpxWdyTIUgVPmF3htMnKaJV7SoCYrv0Vr07CKnY/TDllkq16B/42Z2a0Rtzrj9
FFbVFBl9sADHWurSs7/AhjbyEy6yPsy0ZUYGhGPQxLv88UAc9qQBNYyM5aX0EOG/
6Hwb3loXL+1nACbo9KladlYLeOP4lrfK5RS3PcSiC2MOHMDGgDOeaT2eftTmaPnb
32k+sheApVTqhHMohHJqnfzH8DhtXpp03UO+P+H9WQ51sEF1aOuJAjMEAQEKAB0W
IQSLQGDKaalzVrLc9VGCO/B861xGmwUCZMFRrAAKCRCCO/B861xGmz3wD/97YcC+
s68F0Z5HeMLZDRYU6E8/0i1D8XY2Zrr3LesLMgWQUkXlLARKP3HTwvQXIGs4uQ+1
YC5rm75caONCtdY7EEK6UnLasQKiFGAaEGxIYTUa43zVrOzKtBcCKy2F8OPBSNGA
A8C/mJdqYTyMNNL/Hi24ObnQa+oXGvue/qjPojXa9M4D9pDpYBxloeSeN0jxYK/E
04tcNVLKf/aK/qZso+CkVoFL8bIL+RCkAAVcR/8AB/RdlJVmU0bH5lLvab3ViPpx
UemMgfAMYl6GK+yfa0iE9jYbgLBVICwxxGytuHLsBeZq/bodv3KHoCxHvrCP6K6v
m5QSk1vVgnnNz4WCjGoMdZUJH87EP1ohcwr85SrdodDiwzl2afCQ2ZThrLE/lCn3
k/qdyvKwggQF2JyWmGaU9lYZnvPYinFLeR3PIW4imw6SwavM12bwpzoboEW9QgPc
fu90nWprIQDNVgjZSBD3wWlFsZWx0Yi23LIc7SR04HLP8EfEfdDoicemreCLx/nt
AYyB1dVs2GYL/V6zkpk6P/RKZ6S0Cb+dHdmTiToGVo745UUEhyckJJMmJDLoIjU0
GHnJQtOqrvN2uRfxMR3bBKDwoLhjS0RYa0wWWW8mbf0es5N/b8CaSGEGNGhlzY/Q
5tz/4oiqc2cfy9iEv1tQu0z8Rdr3IIFbkYfw7w==
=UFMz
-----END PGP SIGNATURE-----`}
        </SyntaxHighlighter>
        <Typography variant="body1">
          <Link href="https://deb.stealth.sh/mirrors/stealth/misc/canary/warrant-canary-0.txt">
            Warrant Canary n.0
          </Link>
          {' - '}
          <Link href="https://cdn.palinuro.dev/canary/warrant-canary-0.txt">Mirror 1</Link>{' '}
          <Link href="https://palinuro.eu-central-1.linodeobjects.com/canary/warrant-canary-0.txt">
            Mirror 2
          </Link>{' '}
        </Typography>
        <Typography variant="body1">
          <Link href="https://deb.stealth.sh/mirrors/stealth/misc/canary/warrant-canary-1.txt">
            Warrant Canary n.1
          </Link>
          {' - '}
          <Link href="https://cdn.palinuro.dev/canary/warrant-canary-1.txt">Mirror 1</Link>{' '}
          <Link href="https://palinuro.eu-central-1.linodeobjects.com/canary/warrant-canary-1.txt">
            Mirror 2
          </Link>{' '}
        </Typography>
        <Typography variant="body1">
          <Link href="https://deb.stealth.sh/mirrors/stealth/misc/canary/warrant-canary-2.txt">
            Warrant Canary n.2
          </Link>
          {' - '}
          <Link href="https://cdn.palinuro.dev/canary/warrant-canary-2.txt">Mirror 1</Link>{' '}
          <Link href="https://palinuro.eu-central-1.linodeobjects.com/canary/warrant-canary-2.txt">
            Mirror 2
          </Link>{' '}
        </Typography>
        <Typography variant="body1">
          <Link href="https://deb.stealth.sh/mirrors/stealth/misc/canary/warrant-canary-3.txt">
            Warrant Canary n.3
          </Link>
          {' - '}
          <Link href="https://cdn.palinuro.dev/canary/warrant-canary-3.txt">Mirror 1</Link>{' '}
          <Link href="https://palinuro.eu-central-1.linodeobjects.com/canary/warrant-canary-3.txt">
            Mirror 2
          </Link>{' '}
        </Typography>
        <Typography variant="body1">
          <Link href="https://deb.stealth.sh/mirrors/stealth/misc/canary/warrant-canary-4.txt">
            Warrant Canary n.4
          </Link>
          {' - '}
          <Link href="https://cdn.palinuro.dev/canary/warrant-canary-4.txt">Mirror 1</Link>{' '}
          <Link href="https://palinuro.eu-central-1.linodeobjects.com/canary/warrant-canary-4.txt">
            Mirror 2
          </Link>{' '}
        </Typography>
        <Typography variant="body1">
          <Link href="https://deb.stealth.sh/mirrors/stealth/misc/canary/warrant-canary-5.txt">
            Warrant Canary n.5
          </Link>
          {' - '}
          <Link href="https://cdn.palinuro.dev/canary/warrant-canary-5.txt">Mirror 1</Link>{' '}
        </Typography>
      </Grid>
    </Grid>
  )
}

export default Warrant
