---
title: Stealth 3.4 released
date: 2017-01-04T00:07:19+00:00
author: palinuro
image: /assets/blog/macaw-poly2.jpg
---
Stealth 3.4.1 was released.

It was divided into 2 releases, infact we have released stealth 3.4 on january 1, and then the 3.4.1 release to fix a minor bug to the installer as you can see from our changelog.

&nbsp;

We haven&#8217;t done big changes since the previous release (stealth 3.3) but we have improved several things, infact as we announced for the previous version, stealth 3.3 was just a working snapshot (considered as a preview) of the major work we were doing on stealth 3.4, that we were able to finish earlier than expected.

&nbsp;

Feel free to read our [changelog](https://github.com/StealthOS/changelog/tree/master) to have a better overview of what we have done.

&nbsp;

Of course a big question that many of you may have is: what happened to Stealth Cloud?
  
No, we don&#8217;t have stopped its development, we have just discovered how pointless it was to provide our Cloud edition through a live ISO.
  
Infact we still provide it and you can still get a working stealth cloud environment in 3 ways: with our netboot images (tasksel supports the stealth-cloud metapackages), with our alternate installation script (to install stealth on top of a debian system) and by buying a pre-configured VPS from dasaweb (the official Stealth Cloud VPS provider).

&nbsp;

feel free to [join our community](https://docs-stealthos.khulnasoft.com/community) and give us your feedback as we are up to take a long pause (we will continue to provide system updates through our repositories) to collect new ideas and play with some experimental branches of the IT security universe.