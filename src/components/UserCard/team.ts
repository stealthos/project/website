export default [
  {
    name: 'Lorenzo Faletra',
    nickname: 'palinuro',
    role: 'Team Leader, Core Dev, Infrastructure Manager, Release Manager',
    github: 'https://github.com/PalinuroSec',
    twitter: 'https://twitter.com/palinurosec',
    linkedIn: 'https://linkedin.com/in/lorenzofaletra',
    email: 'mailto:palinuro@khulnasoft.com'
  },
  {
    name: 'Nikos Fountas',
    nickname: 'nfou',
    role: 'SVP Operations at Hack The Box, Director and Strategic Advisor at Stealth Sec',
    github: '',
    twitter: '',
    linkedIn: 'https://www.linkedin.com/in/nfou/',
    email: ''
  },
  {
    name: 'Emmanouil Gavriil',
    nickname: 'arkanoid',
    role: 'VP Content at Hack The Box and Director and Strategic Advisor at Stealth Sec',
    github: '',
    twitter: 'https://twitter.com/manosgavriil/',
    linkedIn: 'https://www.linkedin.com/in/emmanouilgavriil/',
    email: ''
  },
  {
    name: 'Irene Pirrotta',
    nickname: 'Tissy',
    role: 'Community Manager',
    github: 'https://github.com/tissy9491/',
    twitter: 'https://twitter.com/tissy_p',
    linkedIn: 'https://linkedin.com/in/irene-pirrotta',
    email: 'mailto:tissy@khulnasoft.com'
  },
  {
    name: 'Nông Hoàng Tú',
    nickname: 'Dm Knght',
    role: 'Core Dev, Pentest Tool Developer',
    github: 'https://github.com/dmknght',
    twitter: 'https://twitter.com/dmknght1',
    linkedIn: '',
    email: 'mailto:dmknght@khulnasoft.com'
  },
  {
    name: 'Dario Camonita',
    nickname: 'danterolle',
    role: 'Core Dev, Website and Documentation Maintainer',
    github: 'https://github.com/danterolle',
    twitter: 'http://twitter.com/danterolle_',
    linkedIn: 'https://linkedin.com/in/dario-c-74a887149',
    email: 'mailto:danterolle@khulnasoft.com'
  },
  {
    name: 'Giulia M. Stabile',
    nickname: 'sh4rk',
    role: 'Social Media and Content Specialist',
    github: '',
    twitter: 'http://twitter.com/giuliamstabile',
    linkedIn: 'https://www.linkedin.com/in/giulia-michelle-stabile-9320a6252/',
    email: 'mailto:sh4rk@khulnasoft.com'
  }
]
