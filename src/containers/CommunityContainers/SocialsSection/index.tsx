import { Box, Grid, IconButton, Paper, Typography } from '@mui/material'
import { makeStyles } from '@mui/styles'

import Discord from './assets/discord.svg'
import Facebook from './assets/facebook.svg'
import Instagram from './assets/instagram.svg'
import LinkedIn from './assets/linkedin.svg'
/* import Twitch from './assets/twitch.svg' */
import Reddit from './assets/reddit.svg'
import Twitter from './assets/twitter.svg'
import YouTube from './assets/youtube.svg'

const useStyles = makeStyles(theme => ({
  paper: {
    width: '100%',
    padding: theme.spacing(8),
    marginTop: theme.spacing(4),
    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(4)
    }
  },
  iconWrapper: {
    marginTop: theme.spacing(6),
    gap: theme.spacing(4)
  },
  icon: {
    fill: theme.palette.mode === 'dark' ? '#FFFFFF' : '#06043E'
  }
}))

const SocialsSection = () => {
  const classes = useStyles()
  return (
    <>
      <Grid item container xs={12} md={9}>
        <Paper className={classes.paper} elevation={0}>
          <Grid container alignItems="center" direction="column">
            <Typography variant="h5" paragraph>
              Join Us On Social Media
            </Typography>
            <Typography variant="subtitle2Semi">
              Choose the channel that you are most active in and let’s stay connected!
            </Typography>
          </Grid>
          <Box
            className={classes.iconWrapper}
            display="flex"
            justifyContent="center"
            flexWrap="wrap"
          >
            <IconButton href="https://www.instagram.com/stealthproject/">
              <Instagram className={classes.icon} />
            </IconButton>
            <IconButton href="https://twitter.com/stealthos">
              <Twitter className={classes.icon} />
            </IconButton>
            <IconButton href="https://www.facebook.com/StealthOS/">
              <Facebook className={classes.icon} />
            </IconButton>
            <IconButton href="https://discord.gg/j7QTaCzAsm">
              <Discord className={classes.icon} />
            </IconButton>
            <IconButton href="https://www.linkedin.com/company/stealthos/about/">
              <LinkedIn className={classes.icon} />
            </IconButton>
            <IconButton href="https://www.youtube.com/@stealthos/featured">
              <YouTube className={classes.icon} />
            </IconButton>
            <IconButton href="https://www.reddit.com/r/stealthos/">
              <Reddit className={classes.icon} />
            </IconButton>
            {/*
            <IconButton>
              <Twitch className={classes.icon} />
            </IconButton>
            */}
          </Box>
        </Paper>
      </Grid>
    </>
  )
}

export default SocialsSection
