import { Grid } from '@mui/material'
import { makeStyles } from '@mui/styles'

import stealthHome1 from './assets/stealth-home-1.png'
import stealthHome2 from './assets/stealth-home-2.png'
import stealthHome3 from './assets/stealth-home-3.png'
import stealthHome4 from './assets/stealth-home-4.png'
import stealthHome5 from './assets/stealth-home-5.png'

import DESection from 'containers/DownloadContainers/DESection'

const useStyles = makeStyles(theme => ({
  desktopEnvironment: {
    marginTop: theme.spacing(8)
  }
}))

const OSHome = () => {
  const classes = useStyles()
  return (
    <>
      <Grid container justifyContent="center">
        <DESection
          className={classes.desktopEnvironment}
          name="Home Edition"
          description={
            <>
              This edition is a general purpose operating system with the typical Stealth look and
              feel. It is designed for daily use, privacy and software development. Stealth Tools
              can be manually installed to assemble a custom and lightweight pentesting environment.
              It is available for amd64 architectures and also in OVA format (amd64 only). Stealth
              5.3 for UTM is currently available for download.
            </>
          }
          version="6.0 Lorikeet"
          releaseDate="Jan 24, 2024"
          architecture="amd64, arm64"
          screenshots={[stealthHome1, stealthHome2, stealthHome3, stealthHome4, stealthHome5]}
          requirements={[
            { heading: 'Processor', description: 'Dual Core CPU' },
            { heading: 'Graphics', description: 'No Graphical Acceleration Required' },
            { heading: 'Memory', description: '1 GB RAM' },
            { heading: 'Storage', description: '16 GB available space' }
          ]}
          features={[
            {
              hero: 'Workstation',
              content: [
                {
                  heading: 'Full Office Suite',
                  description: (
                    <>
                      Pre-installed LibreOffice, and possibility to install other softwares via the
                      Synaptic package manager.
                    </>
                  )
                },
                {
                  heading: 'Multimedia Production',
                  description: (
                    <>
                      VLC, GIMP and a whole repository from which to install other software such as
                      OBS, Blender, Kdenlive, Krita and more!
                    </>
                  )
                }
              ]
            },
            {
              hero: 'Privacy',
              content: [
                {
                  heading: 'Anonymity tools',
                  description: <>AnonSurf, TOR, Firefox pre-installed Ad-blockers.</>
                },
                {
                  heading: 'Cryptography',
                  description: (
                    <>
                      Full disk encryption and all encryption tools including zulucrypt, sirikali...
                      at your fingertips!
                    </>
                  )
                }
              ]
            },
            {
              hero: 'Development',
              content: [
                {
                  heading: 'Development Tools',
                  description: <>VSCodium and Geany. You can start developing what you want.</>
                },
                {
                  heading: 'Advanced Framework Support',
                  description: (
                    <>
                      Fully support for a lot of programming languages/frameworks like Go, Rust,
                      Python and more.
                    </>
                  )
                }
              ]
            }
          ]}
          downloadOption={{
            iso: 'https://deb.stealth.sh/stealth/iso/6.0/Stealth-home-6.0_amd64.iso',
            virtualbox: 'https://deb.stealth.sh/stealth/iso/5.3/Stealth-home-5.3_amd64.ova',
            utm: 'https://deb.stealth.sh/stealth/iso/5.3/Stealth-home-5.3_arm64.utm.zip'
          }}
          torrent={{
            iso: 'https://deb.stealth.sh/stealth/iso/6.0/Stealth-home-6.0_amd64.iso.torrent',
            virtualbox: 'https://deb.stealth.sh/stealth/iso/5.3/Stealth-home-5.3_amd64.ova.torrent'
          }}
          allHashes={{ url: 'https://deb.stealth.sh/stealth/iso/6.0/signed-hashes.txt' }}
        />
      </Grid>
    </>
  )
}

export default OSHome
