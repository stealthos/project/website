import { Box, Divider, Grid, Link, Paper, Typography } from '@mui/material'
import { makeStyles } from '@mui/styles'
import cls from 'classnames'

import PButton from 'components/PButton'

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    padding: theme.spacing(8),
    [theme.breakpoints.down('md')]: {
      padding: theme.spacing(4)
    }
  },
  desktopEnvironment: {
    marginTop: theme.spacing(8)
  },
  gridHrMarginTop: {
    marginTop: 30,
    marginBottom: 30
  },
  image: {
    display: 'block',
    margin: 'auto',
    paddingBottom: theme.spacing(2)
  },
  wslLink: {
    zIndex: 1,
    position: 'relative'
  }
}))

const OSWsl = () => {
  const classes = useStyles()

  return (
    <>
      <Grid container justifyContent="center">
        <Paper className={cls(classes.root, classes.desktopEnvironment)} elevation={0}>
          <Typography variant="h4" paragraph>
            Windows Subsystem for Linux (WSL)
          </Typography>
          <Typography variant="subtitle2Semi" paragraph>
            Stealth also comes to WSL. Experience the full power of our operating system running
            under Windows! Compatible with Windows 10 and 11 (x86_64).
          </Typography>
          <Grid className={classes.gridHrMarginTop} item xs={12}>
            <Divider variant="fullWidth" />
          </Grid>
          <Grid container justifyContent="center" spacing={2} style={{ marginTop: 20 }}>
            <Grid item xs={12} md={8} justifyContent="center">
              <Box display="flex" flexDirection="column" style={{ gap: 10 }}>
                <Typography variant="h6">0.0.6</Typography>
                <Typography variant="body1Semi">
                  This version is a preview and may have some stability issues.{' '}
                  <Link
                    className={classes.wslLink}
                    href="https://stealthos.khulnasoft.com/docs/installation/install-with-wsl"
                    underline="none"
                  >
                    Check out the documentation to learn more
                  </Link>
                  .
                </Typography>
              </Box>
            </Grid>
            <Grid item xs={12} md={4} justifyContent="center">
              <Box display="flex" flexDirection="column" style={{ gap: 10 }}>
                <PButton
                  gradient
                  variant="contained"
                  to="https://gitlab.com/stealthos/project/wsl/-/jobs/5853355181/artifacts/download"
                >
                  Download
                </PButton>
              </Box>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </>
  )
}

export default OSWsl
