export default [
  {
    name: 'Facebook',
    link: 'https://www.facebook.com/StealthOS'
  },
  {
    name: 'Twitter',
    link: 'https://twitter.com/StealthOS'
  },
  {
    name: 'Instagram',
    link: 'https://www.instagram.com/stealthproject'
  },
  {
    name: 'YouTube',
    link: 'https://www.youtube.com/c/stealthos'
  },
  {
    name: 'LinkedIn',
    link: 'https://www.linkedin.com/company/stealthos'
  },
  {
    name: 'Telegram',
    link: 'https://t.me/stealthosgroup'
  },
  {
    name: 'Discord',
    link: 'https://discord.gg/j7QTaCzAsm'
  },
  {
    name: 'Read our latest article',
    link: 'https://www.stealthos.khulnasoft.com/blog/'
  },
  {
    name: 'Reddit',
    link: 'https://www.reddit.com/r/stealthos/'
  }
]
