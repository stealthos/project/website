export default [
  // NCSA
  {
    url: 'https://mirrors.mit.edu/stealth/',
    lat: 42.3651,
    lon: -71.1045,
    commentary: 'SIPB MIT (1Gbps)',
    id: 'ncsa.mit'
  },
  {
    url: 'https://mirror.clarkson.edu/stealth/',
    lat: 44.6548,
    lon: -74.9555,
    commentary: 'Clarkson University',
    id: 'ncsa.clarkson'
  },
  {
    url: 'https://ftp.osuosl.org/pub/stealthos',
    lat: 43.0718,
    lon: -89.5114,
    commentary: 'Oregon State University - Open Source Lab',
    id: 'ncsa.osuosl'
  },
  {
    url: 'http://mirrors.ocf.berkeley.edu/stealth',
    lat: 37.8574,
    lon: -122.2449,
    commentary: 'Berkeley Open Computing Facility',
    id: 'ncsa.berkeley'
  },
  {
    url: 'https://mirror.wdc1.us.leaseweb.net/stealth',
    lat: 38.7999,
    lon: -77.5443,
    commentary: 'Virginia Leaseweb',
    id: 'ncsa.virginia'
  },
  {
    url: 'http://mirror.cedia.org.ec/stealth',
    lat: -2.8833,
    lon: -78.9833,
    commentary: 'RED CEDIA (National research and education center of Ecuador)',
    id: 'ncsa.cedia'
  },
  {
    url: 'http://mirror.uta.edu.ec/stealth',
    lat: -1.2491,
    lon: -78.6168,
    commentary: 'UTA (Universidad Técnica de ambato)',
    id: 'ncsa.uta'
  },
  {
    url: 'http://mirror.ueb.edu.ec/stealth',
    lat: -2.8833,
    lon: -78.9833,
    commentary: 'UEB (Universidad Estatal de Bolivar)',
    id: 'ncsa.ueb'
  },
  {
    url: 'http://sft.if.usp.br/stealth',
    lat: -23.5475,
    lon: -46.6361,
    commentary: 'University of Sao Paulo',
    id: 'ncsa.usp'
  },
  {
    url: 'https://mirror.0xem.ma/stealth/',
    lat: 45.9454,
    lon: -66.6656,
    commentary: '0xem',
    id: 'ncsa.0xem'
  },
  {
    url: 'https://mirror.linux.ec/stealth',
    lat: -0.2298,
    lon: -78.525,
    commentary: 'EcuaLinux',
    id: 'ncsa.ecualinux'
  },
  // EMEA
  {
    url: 'http://stealth.mirror.garr.it/mirrors/stealth',
    lat: 41.1177,
    lon: 16.8512,
    commentary: 'GARR Consortium (Italian Research & Education Network)',
    id: 'emea.garr'
  },
  {
    url: 'http://ftp.halifax.rwth-aachen.de/stealthos',
    lat: 50.7766,
    lon: 6.0834,
    commentary: 'RWTH-Aachen (Halifax students group)',
    id: 'emea.halifax'
  },
  {
    url: 'http://ftp-stud.hs-esslingen.de/pub/Mirrors/archive.stealthos.khulnasoft.com',
    lat: 48.7823,
    lon: 9.177,
    commentary: 'Esslingen (University of Applied Sciences)',
    id: 'emea.esslingen'
  },
  {
    url: 'https://mirror.fra10.de.leaseweb.net/stealth',
    lat: 50.1155,
    lon: 8.6842,
    commentary: 'Germany Leaseweb',
    id: 'emea.leaseweb'
  },
  {
    url: 'https://stealth.mickhat.xyz',
    lat: 50.4779,
    lon: 12.3713,
    commentary: 'Mickhat, Hetzner',
    id: 'emea.mickhat'
  },
  {
    url: 'https://mirror.pyratelan.org/stealth',
    lat: 49.4478,
    lon: 11.0683,
    commentary: 'pyratelan',
    id: 'emea.pyratelan'
  },
  {
    url: 'http://ftp.nluug.nl/os/Linux/distr/stealth',
    lat: 52.0908,
    lon: 5.1222,
    commentary: 'Nluug',
    id: 'emea.nluug'
  },
  {
    url: 'https://mirror.lyrahosting.com/stealth',
    lat: -4.6167,
    lon: 55.45,
    commentary: 'lyrahosting',
    id: 'emea.lyrahosting'
  },
  {
    url: 'http://ftp.acc.umu.se/mirror/stealthos.khulnasoft.com/stealth',
    lat: 63.8284,
    lon: 63.8284,
    commentary: 'ACC UMU (Academic Computer Club, Umea University)',
    id: 'emea.umu'
  },
  {
    url: 'http://ftp.cc.uoc.gr/mirrors/linux/stealth',
    lat: 38.0667,
    lon: 23.7667,
    commentary: 'UoC (University of Crete - Computer Center)',
    id: 'emea.uoc'
  },
  {
    url: 'http://ftp.belnet.be/mirror/archive.stealthos.khulnasoft.com',
    lat: 50.8504,
    lon: 4.3488,
    commentary: 'Belnet (The Belgian National Research)',
    id: 'emea.belnet'
  },
  {
    url: 'https://matojo.unizar.es/stealth/',
    lat: 41.6561,
    lon: -0.8773,
    commentary: 'Osluz (Oficina de software libre de la Universidad de Zaragoza)',
    id: 'emea.osluz'
  },
  {
    url: 'https://mirrors.up.pt/stealth/',
    lat: 41.1931,
    lon: -8.5817,
    commentary: 'U.Porto (University of Porto)',
    id: 'emea.up'
  },
  {
    url: 'https://mirrors.dotsrc.org/stealth/',
    lat: 55.7704,
    lon: 12.5038,
    commentary: 'Dotsrc (Aalborg university)',
    id: 'emea.dotsrc'
  },
  {
    url: 'https://stealth.mirror.cythin.com/stealth',
    lat: 50.6942,
    lon: 3.1746,
    commentary: 'cythin.com',
    id: 'emea.cythin'
  },
  {
    url: 'https://stealth-mirror.iriseden.eu/stealth',
    lat: 60.1695,
    lon: 24.9354,
    commentary: 'iriseden',
    id: 'emea.iriseden'
  },
  {
    url: 'https://quantum-mirror.hu/mirrors/pub/stealth/',
    lat: 48.1,
    lon: 20.7833,
    commentary: 'quantum-mirror.hu',
    id: 'emea.quantum'
  },
  {
    url: 'http://turkey.archive.stealthos.khulnasoft.com/stealth',
    lat: 41.0138,
    lon: 28.9497,
    commentary: 'Turkey EB',
    id: 'emea.turkey'
  },
  {
    url: 'https://mirror.cspacehostings.com/stealthos/',
    lat: 59.437,
    lon: 24.7535,
    commentary: 'cspacehosting',
    id: 'emea.cspacehosting'
  },
  {
    url: 'https://mirror.yandex.ru/mirrors/stealth/',
    lat: 55.7522,
    lon: 37.6156,
    commentary: 'Yandex Mirror',
    id: 'apac.yandex'
  },
  {
    url: 'https://mirror.truenetwork.ru/stealth/',
    lat: 55.0415,
    lon: 82.9346,
    commentary: 'Truenetwork',
    id: 'apac.truenetwork'
  },
  {
    url: 'https://mirror.surf/stealth/',
    lat: 56.3287,
    lon: 44.002,
    commentary: 'surf',
    id: 'emea.surf'
  },
  {
    url: 'http://mirrors.comsys.kpi.ua/stealth/',
    lat: 50.4547,
    lon: 30.5238,
    commentary: 'KPI (National Technical University of Ukraine - Comsys)',
    id: 'emea.comsys'
  },
  {
    url: 'https://stealth.astra.in.ua/',
    lat: 49.8383,
    lon: 24.0232,
    commentary: 'astra.in.ua',
    id: 'emea.astra'
  },
  {
    url: 'https://mirror.sagorski.org/stealth/',
    lat: 50.6049,
    lon: 11.0357,
    commentary: 'Germany, Karlsruhe, Sagorski',
    id: 'emea.sagorski'
  },
  // APAC
  {
    url: 'http://mirror.amberit.com.bd/stealthos/',
    lat: 23.7104,
    lon: 90.4074,
    commentary: 'Amberit (Dhakacom)',
    id: 'apac.amberit'
  },
  {
    url: 'http://free.nchc.org.tw/stealth/',
    lat: 25.0478,
    lon: 121.5319,
    commentary: 'NCHC (Free Software Lab)',
    id: 'apac.nchc'
  },
  {
    url: 'https://mirror.0x.sg/stealth/',
    lat: 1.2897,
    lon: 103.8501,
    commentary: '0x',
    id: 'apac.0x'
  },
  {
    url: 'http://mirrors.ustc.edu.cn/stealth/',
    lat: 31.8639,
    lon: 117.2808,
    commentary: 'University of Science and Technology of China and USTCLUG',
    id: 'apac.ustc'
  },
  {
    url: 'https://mirrors.tuna.tsinghua.edu.cn/stealth/',
    lat: 39.9115,
    lon: 116.3603,
    commentary: 'TUNA (Tsinghua university of Beijing, TUNA association)',
    id: 'apac.tuna'
  },
  {
    url: 'http://mirrors.sjtug.sjtu.edu.cn/stealth/',
    lat: 31.2222,
    lon: 121.4581,
    commentary: 'SJTUG (SJTU *NIX Users Group)',
    id: 'apac.sjtug'
  },
  {
    url: 'https://mirror.lagoon.nc/pub/stealth/',
    lat: -22.2763,
    lon: 166.4572,
    commentary: 'Lagoon',
    id: 'apac.lagoon'
  },
  {
    url: 'https://mirror.kku.ac.th/stealth/',
    lat: 13.75,
    lon: 100.5167,
    commentary: 'KKU (Khon Kaen University)',
    id: 'apac.kku'
  },
  {
    url: 'http://kartolo.sby.datautama.net.id/stealth/',
    lat: -7.2492,
    lon: 112.7508,
    commentary: 'Datautama',
    id: 'apac.datautama'
  },
  {
    url: 'https://mirrors.nxtgen.com/stealth-mirror/',
    lat: 28.66667,
    lon: 77.21667,
    commentary: 'NxtGen DataCenter & Cloud Technologies',
    id: 'apac.nxtgen'
  },
  {
    url: 'https://mirror.yer.az/stealth/',
    lat: 40.3777,
    lon: 49.892,
    commentary: 'YER Hosting',
    id: 'apac.yer'
  }
]
