import Hoodie from './assets/hoodie.png'
import Sticker1 from './assets/sticker-1.png'
import Sticker3 from './assets/sticker-3.png'
import Shirt1 from './assets/t-shirt-1.png'
import Shirt2 from './assets/t-shirt-2.png'
import Shirt3 from './assets/t-shirt-3.png'

export default [Hoodie, Sticker1, Shirt2, Sticker3, Shirt1, Shirt3]
