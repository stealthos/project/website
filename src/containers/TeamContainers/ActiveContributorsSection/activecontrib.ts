export default [
  {
    nickname: 'terabreik',
    name: 'José Gatica',
    role: 'Spanish Community Leader, Responsible for the Spanish documentation',
    email: 'josegatica at stealthos dot org'
  },
  {
    nickname: 'anubi5',
    name: 'Abdel Rhman Anter',
    role: 'Arabic Community Leader, Forum Moderator',
    email: 'anubi5 at stealthos dot org'
  },
  {
    nickname: 'Jeff',
    name: 'Jeff Szydel',
    role: 'Facebook admin, System Tester',
    email: 'jeff at stealthos dot org'
  },
  {
    nickname: 'PTD',
    name: 'Patrick Dunn',
    role: 'StealthOS Global Telegram admin, Documentation contributor',
    email: 'ptd at stealthos dot org'
  },
  {
    nickname: 'XC0D3',
    name: 'Raul Alderete',
    role: '',
    email: ''
  },
  {
    nickname: 'Serverket',
    name: 'Manuel Hernandez',
    role: 'Graphic Designer, Web Dev contributor',
    email: ''
  },
  {
    nickname: 'janopineda',
    name: 'Alejandro Pineda',
    role: '',
    email: ''
  },
  {
    nickname: 'botnetsec',
    name: 'jhon delgado',
    role: 'Spanish documentation contributor',
    email: ''
  },
  {
    nickname: 'Shashank257',
    name: 'SHASHANK R R',
    role: 'Indian Community Leader',
    email: ''
  }
]
