export default [
  {
    nickname: 'Kratvers',
    name: 'Denis',
    role: 'Russian Community Leader, Forum Moderator',
    email: ''
  },
  {
    nickname: 'ku3k3n',
    name: '',
    role: 'German Community Leader, Forum Moderator',
    email: ''
  },
  {
    nickname: 'Nico_Paul',
    name: 'Nico Gialluca',
    role: 'English Community Leader, Forum Moderator, Organizer',
    email: 'nico at stealthos dot org'
  },
  {
    nickname: 'ByteHackr',
    name: 'Sandipan Roy(NEMO)',
    role: 'Security Engineer',
    email: 'sandipan at stealthos dot org'
  },
  {
    nickname: 'Mr-B Relax',
    name: 'Bimantara Sutato Putra',
    role: 'Indonesian Community Leader, Forum Moderator',
    email: ''
  },
  {
    nickname: 'Jarfr',
    name: 'Jérôme Brichese',
    role: 'French Community Leader, Forum Moderator',
    email: 'jarfr at stealthos dot org'
  },
  {
    nickname: 'AresX',
    name: '',
    role: 'Chinese Community Leader, Forum Moderator',
    email: ''
  },
  {
    nickname: 'meu',
    name: 'M. Emrah Ünsür',
    role: 'Turkish Community Leader, Forum Moderator',
    email: 'meu at stealthos dot org'
  },
  {
    nickname: 'nikksno',
    name: 'Nicolas North',
    role: 'Sysadmin, legal support',
    email: 'nz at os dot vu'
  },
  {
    nickname: 'Andi',
    name: '',
    role: 'Dev',
    email: 'nz at os dot vu'
  },
  {
    nickname: '',
    name: 'Tiago Teixeira',
    role: '',
    email: ''
  },
  {
    nickname: '',
    name: 'Andrea Costa',
    role: 'Website contributor',
    email: ''
  },
  {
    nickname: 'Quietwalker',
    name: 'Emanuel Di Vita',
    role: 'Former free-software licensing consultant',
    email: ''
  },
  {
    nickname: 'Paal',
    name: '',
    role: 'Former Forum setup, Organizer, Graphic Design',
    email: 'paal at stealthos dot org'
  },
  {
    nickname: 'sheireen',
    name: 'Lisetta Ferrero',
    role: 'Former Core Dev',
    email: 'sheireen at stealthos dot org'
  },
  {
    nickname: 's1udge',
    name: '',
    role: 'Operations lead, Forum Admin, Docs writer, Dev',
    email: ''
  },
  {
    nickname: 'qr0f1l3r',
    name: 'Andre Patock',
    role: 'German Community Leader, Forum Moderator',
    email: 'andrepatock at stealthos dot org'
  },
  {
    nickname: 'mibofra',
    name: 'Francesco Bonanno',
    role: 'ARM Contributor',
    email: 'mibofra at stealthos dot org'
  },
  {
    nickname: 'gnugr',
    name: 'Vangelis Mouhtsis',
    role: 'Debian/MATE dev',
    email: 'gnugr at stealthos dot org'
  },
  {
    nickname: 'h0tw4t3r',
    name: 'Vladyslav Dalechyn',
    role: 'Website Maintainer',
    email: ''
  },
  {
    nickname: 'mastrobirraio',
    name: 'Pino Matranga',
    role: 'Italian Community Leader, Forum Moderator',
    email: ''
  },
  {
    nickname: 'KileXt',
    name: 'Kilian',
    role: 'Responsible for the French documentation',
    email: ''
  }
]
